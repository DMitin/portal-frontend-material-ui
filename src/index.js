// @flow

import React from 'react';
import { render } from 'react-dom';
import Index from './pages/index';
import Login from './pages/login'
import Grid from './grid/index';
import MainPage from './grid/mainpage';
import CenteredGrid from './grid/centered';

import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";

render(<Router>
    <App />
</Router>, document.querySelector('#root'));

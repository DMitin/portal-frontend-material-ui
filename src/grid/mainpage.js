// @flow weak

import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';

import Skills from '../chips/chips1'

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: 30,
  },
  paper: {
    padding: 16,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class MainPage extends Component {

  componentDidMount() {
    this.props.history.push("/login");
  }


  render() {

    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={3}>
            <Paper >
              <img src="http://loremflickr.com/200/300"></img>
                </Paper>
              </Grid>
              <Grid item xs={9}>
                <Paper >
                  <Skills/>
                  <Divider/>
                  <Skills/>
                  <Divider/>
                  <Skills/>
                </Paper>
              </Grid>
            </Grid>
          </div>
      );
  }

}

MainPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MainPage);

import React from "react";
import { Route, Switch } from "react-router-dom";

import Login from './pages/login'
import Main from './grid/mainpage'

export default () =>
  <Switch>
    <Route path="/" exact component={Main} />
    <Route path="/login" exact component={Login} />
  </Switch>;
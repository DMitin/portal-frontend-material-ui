/* eslint-disable no-undef */
function skill(cb) {
  return fetch("/hello/", {
    accept: "application/json"
  })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb);
}




function checkStatusLogin(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
}


function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  error.status = response.statusText;
  error.response = response;
  console.log(error); // eslint-disable-line no-console
  throw error;
}

function parseJSON(response) {
  return response.json();
}

const Test = { skill, login };
export default Test;

function login(username, password, cb) {
  var formData = new FormData();
  formData.append("username", username);
  formData.append("password", password);
  return fetch("/login", {
    method: "POST",
    body: formData
  })
    .then(cb);
}

function isLogined(cb) {
  return fetch("/hello/isLogined")
    .then(cb);
}

const Authentication = {login, isLogined};
export default Authentication;

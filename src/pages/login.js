import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'material-ui/styles/withStyles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';


//import TestService from '../service/test'
import AuthenticationService from '../service/authentication'
import LoginMessage from '../sbackbar/loginsnackbar'


const styles = {
  root: {
    textAlign: 'center',
    paddingTop: 200,
  },
  textField: {
    width: 200,
  },
};

const styles_new = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});


class Login extends Component {

  constructor(props){
    super(props);
    this.state={
      username:'',
      password:'',
      open: false,
      message: ""
    }
  }

  handleLogin (event){
    AuthenticationService.login(this.state.username, this.state.password, response => {
      console.log("auth");
      if(response.status == 401) {
        this.setState({
          open: true,
          message: "Error to connect"
        });
      } else if (response.status == 200) {
        this.props.history.push("/main");
      } else {
        this.setState({
          open: true,
          message: "Server error"
        });
      }
    })
  }

  setPassword (password) {
    this.setState({password: password});
  }

  setUsername (username) {
    this.setState({username: username});
  }

  render() {
    return (
      <div className={this.props.classes.root}>
        <div>
          <TextField
            id="login"
            label="login"
            type="login"
            autoComplete="current-password"
            margin="normal"
            onChange={(event) => this.setUsername(event.target.value)}
          />
        </div>
        <div>
          <TextField
            id="password"
            label="Password"
            type="password"
            autoComplete="current-password"
            margin="normal"
            onChange ={(event) => this.setPassword(event.target.value)}
          />
        </div>
        <div>
          <Button raised color="primary" onClick={(event) => this.handleLogin(event)}>
            Login
          </Button>
            <LoginMessage open={this.state.open} message={this.state.message}/>
        </div>
      </div>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);
